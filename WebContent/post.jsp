<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp" flush="true" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
</head>
<body>

	<div class="main-contents">
		<div class="header">
			<c:if test="${ not empty loginUser }">
			</c:if>
			<c:if test="${ not empty loginUser }">
				<div class="profile">
					<div class="name">
						<h2>
							<c:out value="${loginUser.name}" />
						</h2>
					</div>
					<div class="account">

						<c:out value="${loginUser.account}" />
					</div>
				</div>
			</c:if>

		</div>
		<div class="form-area">
		<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="messages">
					<li><c:out value="${messages}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

			<form action="./post" method="post">
				件名<br /> <input name="subject" class="Post-box">（30文字まで）<br />
				カテゴリー<br /> <input name="category" class="Post-box">（10文字まで）<br />
				投稿<br />
				<textarea name="text" cols="100" rows="10" class="Post-box"></textarea>
				<br /> <input type="submit" value="投稿">（1000文字まで）
			</form>
		</div>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"><c:out value="${message.account}" /></span>
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text">
						<c:out value="${message.text}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>