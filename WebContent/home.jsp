

<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp" flush="true" />
<head>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
<link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon">
</head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>掲示板ホーム画面</title>

</head>
<body>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					こんにちは！
					<c:out value="${loginUser.name}" />
					さん
				</h2>
				<span class="account"> ID: <c:out
						value="${loginUser.account}" />
				</span>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</c:if>
	<div class="main-contents">

		<div class="header">




		</div>
	</div>
	<div class="posts">

		<form action="index.jsp" method="get">
			<fieldset>
				<legend>投稿検索 </legend>
				<div>
					<label for="start">Start</label> <input type="date" id="start"
						name="trip1" value="" min="2018-01-01" max="2018-12-31" />
				</div>

				<div>
					<label for="end">End</label> <input type="date" id="end"
						name="trip2" value="" min="2018-01-01" max="2018-12-31" />
				</div>
				<input name="category" class="Post-box" placeholder="カテゴリー検索">
				<label for="search"></label><input type="submit" value="検索">
			</fieldset>
		</form>
		<form action="./" method="get">
			<input type="submit" value="投稿全表示">
		</form>

		<c:forEach items="${posts}" var="post">
			<div class="post">
				<div class="account-name"></div>
				<div class="id">
					投稿No.
					<c:out value="${post.id}" />
				</div>
				<div class="name">
					投稿者：
					<c:out value="${post.name}" />
				</div>
				<div class="subject">
					件名：
					<c:out value="${post.subject}" />
				</div>
				<div class="category">
					カテゴリー：
					<c:out value="${post.category}" />
				</div>
				<div class="text">
					投稿：
					<c:out value="${post.text}" />
				</div>
				<div class="date">
					投稿日時：
					<fmt:formatDate value="${post.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
				<form action="deletePost" method="post">
					<c:if test="${ loginUser.id == post.userId }">
						<input type="submit" value="この投稿を削除">
						<input name="postId" value="${post.id}" id="id" type="hidden" />
					</c:if>
				</form>
			</div>
			<form action="newComment" method="post">
				<input name="postId" value="${post.id}" id="id" type="hidden" />
				<div class="comments">
					コメント<br />
					<textarea name="text" cols="150" rows="10" class="Post-box"></textarea>
					<br /> <input type="submit" value="コメント投稿">（500文字まで）
				</div>
				<br>
			</form>
			<c:forEach items="${comments}" var="comment">
				<c:if test="${post.id == comment.postId }">
					<div class="display-message">

						<span class="name"><c:out value="${comment.name }" /></span>
						<div class="text">
							<c:out value="${comment.text}" />
						</div>
						<form action="deleteComment" method="post">
							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<div>
								<c:if test="${ loginUser.id == comment.userId }">
									<input type="submit" value="このコメントを削除">
									<input name="commentId" value="${comment.id}" id="id"
										type="hidden" />
								</c:if>
								<br>
							</div>
							<br>
						</form>
					</div>
				</c:if>
			</c:forEach>
			<br>
		</c:forEach>
	</div>
</body>
</html>
