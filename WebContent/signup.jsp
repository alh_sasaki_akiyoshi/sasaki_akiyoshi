<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp" flush="true" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="account">ログインID</label> <input name="account" value="${signupUser.account}" id="name" /><br />

				<label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
				<label for="name">氏名</label> <input name="name" value="${signupUser.name}" id="name" /> <br />

				<p><select name="branchSelect" size="1">
				<c:forEach items="${branches}" var="branches">
				<option value="${branches.id}" label="${branches.name}"></option>
				</c:forEach>
				</select></p>

				<p><select name="positionSelect" size="1">
				<c:forEach items="${positions}" var="positions">
				<option value="${positions.id}" label="${positions.name}"></option>
				</c:forEach>
				</select></p>

				<label for="isDeleted"></label><input name="isDeleted" value="${signupUser.positionId}" id="isDeleted"  type="hidden">
				 <br />
				 <br />
				 <input type="submit" value="登録" /> <br />
		</form>
	</div>
</body>
</html>