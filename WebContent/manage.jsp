<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp" flush="true" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面</title>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<table border="1" width="700" cellspacing="0" cellpadding="5"
		align="center" valign="top" bordercolor="#333333">
		<tr>
			<th bgcolor="#EE0000"><font color="#FFFFFF">登録番号</font></th>
			<th bgcolor="#EE0000" width="150"><font color="#FFFFFF">登録者名</font></th>
			<th bgcolor="#EE0000" width="250"><font color="#FFFFFF">支社名</font></th>
			<th bgcolor="#EE0000" width="250"><font color="#FFFFFF">役職名</font></th>
			<th bgcolor="#EE0000" width="100"><font color="#FFFFFF">復活停止</font></th>
			<th bgcolor="#EE0000" width="100"><font color="#FFFFFF">編集</font></th>
		</tr>

		<c:forEach items="${manages}" var="manages">
			<tr>
				<td bgcolor="#99CC00" align="right" nowrap><c:out
						value="${manages.id}" /></td>

				<td bgcolor="#FFFFFF" valign="top" width="250"><c:out
						value="${manages.name}" /></td>

				<td bgcolor="#FFFFFF" valign="top" width="250"><c:forEach
						items="${branches}" var="branches">
						<c:if test="${manages.branchId == branches.id }">
							<c:out value="${branches.name}" />
						</c:if>
					</c:forEach></td>

				<td bgcolor="#FFFFFF" valign="top" width="250"><c:forEach
						items="${positions}" var="positions">
						<c:if test="${manages.positionId == positions.id }">
							<c:out value="${positions.name}" />
						</c:if>
					</c:forEach></td>


				<script type="text/javascript">
						<!--
							function check() {

								if (window.confirm('復活しますか？')) { // 確認ダイアログを表示

									return true; // 「OK」時は送信を実行

								} else { // 「キャンセル」時の処理

									window.alert('キャンセルされました'); // 警告ダイアログを表示
									return false; // 送信を中止

								}

							}
						// -->
						</script>

				<script type="text/javascript">
						<!--
							function check1() {

								if (window.confirm('停止しますか？')) { // 確認ダイアログを表示

									return true; // 「OK」時は送信を実行

								} else { // 「キャンセル」時の処理

									window.alert('キャンセルされました'); // 警告ダイアログを表示
									return false; // 送信を中止

								}

							}
						// -->
						</script>
				<td bgcolor="#FFFFFF" valign="top" width="250" align="center"><c:if
						test="${manages.id != loginUser.id}">
						<form action="isRebooted" method="post" onSubmit="return check()"
							style="display: inline;">
							<c:if test="${manages.isDeleted == 1 }">

								<input name="reboot" value="0" type="hidden" />
								<input name="rebootId" value="${manages.id}" type="hidden" />
								<input type="submit" value="復活">
							</c:if>
						</form>
						<form action="isDeleted" method="post" onSubmit="return check1()"
							style="display: inline;">
							<c:if test="${manages.isDeleted == 0 }">
								<input name="stop" value="1" type="hidden" />
								<input name="stopId" value="${manages.id}" type="hidden" />
								<input type="submit" value="停止">
							</c:if>
						</form>
					</c:if></td>
				<td bgcolor="#FFFFFF" valign="top" width="250" align="center">
					<form action="edit" method="get">
						<input name="editId" value="${manages.id}" type="hidden" /> <input
							type="submit" value="編集">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>