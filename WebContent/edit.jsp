<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="header.jsp" flush="true" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.name}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
</head>
<body>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
					さんが編集しています。
				</h2>
			</div>
			<div class="account">
				ID:
				<c:out value="${loginUser.account}" />
			</div>
		</div>
	</c:if>

	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>

			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="edit" method="post">
			<br /> <input name="editId" value="${editUser.id}" id="id" type="hidden" />
			<label for="name">名前</label> <input name="name"
				value="${editUser.name}" id="name" /><br /> <label for="account">アカウントID</label>
			<input name="account" value="${editUser.account}" /><br /> <label
				for="password">パスワード</label> <input name="password1" type="password"
				id="password"  /> <label for="password">パスワード再入力</label> <input
				name="password2" type="password" id="password1" />

			<p>
				<select name="branchSelect" size="1">
					<c:forEach items="${branches}" var="branches">
						<option value="${branches.id}" label="${branches.name}"></option>
					</c:forEach>
				</select>
			</p>

			<p>
				<select name="positionSelect" size="1">
					<c:forEach items="${positions}" var="positions">
						<option value="${positions.id}" label="${positions.name}"></option>
					</c:forEach>
				</select>
			</p>

			<input name="isDeleted" value="${editUser.isDeleted}" type="hidden" />

			<input type="submit" value="登録" /> <br />
		</form>
	</div>
</body>
</html>