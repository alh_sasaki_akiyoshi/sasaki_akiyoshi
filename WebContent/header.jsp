<%@page isELIgnored="false"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="false" %>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
<body>


<style>
nav{
    width: 100%;    /*横幅の指定*/
    border-top: 1px solid blue;   /*上部の線の色を指定*/
    border-bottom: 1px solid blue;    /*下部の線の色を指定*/
    margin-bottom: 5px; /**/
    overflow: hidden;   /*おまじない*/
}
nav ul{
    width: 60%; /*横幅の指定*/
    margin-left: 20%;   /*左端から20%右側に動かす*/
}
nav li{
    width: calc(25% - 2px); /*横幅の指定（線の分をマイナスする）*/
    border-left: 1px solid blue;  /*線を描く*/
    text-align: center; /*文字を中央に*/
    float: left;    /*左から並べる*/
}
nav li:last-child{
    border-right: 1px solid blue; /*li要素の最後の物は右側に線を描く*/
}
nav a{
    display: block; /*1つのli全体にリンクを有効にする*/
    text-decoration: none;  /*ブラウザ標準のリンク装飾をオフに*/
    color:#313131;  /*文字色の変更*/
    font-size: 110%;    /*フォントサイズの指定*/
    letter-spacing: 5px;    /*文字と文字の間隔をあける*/
    font-weight: 400;   /*文字の太さを調整*/
    line-height: 45px;  /*行間の指定（ナビボタンの高さ指定）*/
}
nav a:hover{
    background-color: blue;   /*背景色の指定*/
    color: #fff;    /*文字色の変更*/
    transition: 0.5s;   /*ホバー時の動きをなめらかにする*/
}

header h1{
    font-size:120%; /*フォントサイズの調整*/
    color:#313131; /*文字色の変更*/
    padding-top: 10px; /*文字上部に余白*/
    padding-bottom: 10px; /*文字下部に余白*/
    padding-left: 20px; /*文字左側に余白*/
    padding-right: 20px; /*文字右側に余白*/
    border:1px solid #313131; /*文字の周りに線を描く*/
    border-radius: 5px; /*線の角に丸みを付ける*/
    letter-spacing: 3px; /*文字と文字の間隔をあける*/
    font-weight:400; /*文字の太さの変更*/
}


</style>


<header>
    <h1>Bulletin board</h1>
</header>
<c:out value="${loginUser.name}" />
        <nav>
  <ul style="list-style:none;">
    <li><a href="./">Home</a></li>
    <li><a href="manage">ユーザー管理</a></li>

     <c:if test="${loginUser.positionId == 2 }">
    <li><a href="manage">ユーザー管理</a></li>
    </c:if>

    <li><a href="signup">ユーザー登録</a></li>
    <li><a href="post">投稿</a></li>
    <li><a href="logout">logout</a></li>
  </ul>
</nav>
</body>
    </head>

