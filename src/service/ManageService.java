package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Manage;
import dao.ManageDao;

public class ManageService {
	private static final int LIMIT_NUM = 1000;

	public List<Manage> getManage() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        ManageDao manageDao = new ManageDao();
	        List<Manage> ret = manageDao.getManage(connection, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
