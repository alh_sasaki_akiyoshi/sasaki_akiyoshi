package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.UserPost;
import dao.PostDao;
import dao.UserPostDao;
public class PostService {

	public void register(Post post) {
		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection,post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;


	public List<UserPost> getPost(String category, String stert, String end) {
		Date date = new Date();
		String nowDate = String.valueOf(date);
		String firstDate = "1994-09-28 00:00:00";
		Connection connection = null;
		try{
			if (!(StringUtils.isEmpty(stert) && StringUtils.isEmpty(end)) && stert.equals(end)) {
				String stert1 = " 00:00:00";
				String end1 = " 23:59:59";
				String today1 = stert + stert1;
				String today2 = end + end1;
				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, category,today1,today2);
				commit(connection);
				return ret;
			} else if (StringUtils.isEmpty(stert) && !(StringUtils.isEmpty(end)) && category == null) {
				String wild1 = "";
				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, wild1,firstDate,end);
				return ret;
			}
			if (!(StringUtils.isEmpty(stert) && StringUtils.isEmpty(end)) && category != null) {
				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, category,stert,end);
				commit(connection);

				return ret;
			}else if (!(StringUtils.isEmpty(stert) && StringUtils.isEmpty(end)) && category != null){

				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, category,stert,nowDate);
				commit(connection);
				return ret;
			}
			if (StringUtils.isEmpty(stert) && (StringUtils.isEmpty(end)) && category == null) {
				String wild = "";
				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, wild,firstDate,nowDate);
				commit(connection);

				return ret;

			} else {
				connection = getConnection();
				UserPostDao postDao = new UserPostDao();
				List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, category,firstDate,end);
				return ret;
			}




		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
		} finally {
			close(connection);
		}
		return null;
	}
	public boolean delete(int deletePost) {
		Connection connection = null;
		try {
			connection = getConnection();

			PostDao PostDao = new PostDao();
			PostDao.deletePost(connection,deletePost);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
		return false;
	}
}
