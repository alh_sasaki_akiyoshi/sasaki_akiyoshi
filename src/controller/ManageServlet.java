package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Manage;
import beans.Position;
import beans.User;
import service.BranchService;
import service.ManageService;
import service.PositionService;

@WebServlet(urlPatterns = { "/manage" })
public class ManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowManageForm;
		HttpSession session = request.getSession();
		if (user != null) {
			isShowManageForm = true;
		} else {
			isShowManageForm = false;
		}
		List<Manage> manages = new ManageService().getManage();
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		session.setAttribute("loginUser", user);
		request.setAttribute("manages", manages);
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.setAttribute("isShowMessageForm", isShowManageForm);
		request.getRequestDispatcher("/manage.jsp").forward(request, response);
	}

}