package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;


@WebServlet(urlPatterns = { "/isDeleted" })
public class IsDeletedServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> isDeleted = new ArrayList<String>();
		if (isValid(request, isDeleted) == true) {
			int stop =Integer.parseInt(request.getParameter("stop"));
			int stopId = Integer.parseInt(request.getParameter("stopId"));
			new UserService().userDeleted(stop,stopId);
			response.sendRedirect("manage");
		} else {
			session.setAttribute("errormainComments", isDeleted);
			response.sendRedirect("manage");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> isDeleted) {

		if (isDeleted.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
