package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Manage;
import beans.Position;
import beans.User;
import service.BranchService;
import service.ManageService;
import service.PositionService;
import service.UserService;
@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet{
	public static final long serialVersionUID =1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}
		List<Manage> manages = new ManageService().getManage();
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		session.setAttribute("loginUser", user);
		request.setAttribute("manages", manages);
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		if (inValid(request, messages) == true) {
			int intbranch = Integer.parseInt(request.getParameter("branchSelect"));
			int intPosition = Integer.parseInt(request.getParameter("positionSelect"));
			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(intbranch);
			user.setPositionId(intPosition);
			user.setIsDeleted(0);

			new UserService().register(user);

			messages.add("ユーザーを登録しました");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}
	private boolean inValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchSelect"));
		int positionId = Integer.parseInt(request.getParameter("positionSelect"));

		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else{
			if (!(password.length() >= 6)) {
				messages.add("入力したパスワードは6文字以上で入力してください");
			}
			if (!(password.length() <= 20)) {
				messages.add("入力したパスワードは20文字以内で入力してください");
			}
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}

		if (!(account.matches("^[0-9a-zA-Z]+$"))) {
			messages.add("入力したIDは半角英数字で入力してください");
		}
		if (!(password.matches("^[0-9a-zA-Z]+$"))) {
			messages.add("入力したパスワードは半角英数字で入力してください");
		}


		if ((branchId == 1 && !(positionId == 1 || positionId == 2)) || (branchId == 2 && !(positionId == 3 || positionId == 6))
				|| (branchId == 3 && !(positionId == 4 || positionId == 7)) ||  (branchId == 4 && !(positionId == 5 || positionId == 8))) {
			messages.add("入力した支店と役職は正しくありません");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}