package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/post" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();
		if (isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");
			Post post = new Post();
			post.setSubject(request.getParameter("subject"));
			post.setCategory(request.getParameter("category"));
			post.setText(request.getParameter("text"));
			post.setUserId(user.getId());

			post.setUserId(user.getId());

			new PostService().register(post);
			messages.add("投稿しました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("post");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");
		String text = request.getParameter("text");
		if (StringUtils.isBlank(subject)) {
			messages.add("件名を入力してください");
		}else{
			if (30 < subject.length()) {
				messages.add("件名を30文字以内で入力してください。");
			}
		}
		if (StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		}else{
			if (10 < category.length()) {
				messages.add("カテゴリーを10文字以内で入力してください。");
			}
		}
		if (StringUtils.isBlank(text)) {
			messages.add("本文を入力してください");
		}else{
			if (1000 < text.length()) {
				messages.add("本文を1000文字以内で入力してください。");
			}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
