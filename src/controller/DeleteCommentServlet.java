package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> commentDelete = new ArrayList<String>();
		if (isValid(request, commentDelete) == true) {
			int deleteComment =(Integer.parseInt(request.getParameter("commentId")));
			new CommentService().delete(deleteComment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errormainComments", commentDelete);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> deleteComment) {

		//String subject = request.getParameter("subject");

		if (deleteComment.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
