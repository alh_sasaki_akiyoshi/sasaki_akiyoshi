package controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.PostService;

@WebServlet(urlPatterns = { "/deletePost" })
public class DeletePostServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> postDelete = new ArrayList<String>();
		if (isValid(request, postDelete) == true) {
			int deletePost =(Integer.parseInt(request.getParameter("postId")));
			new PostService().delete(deletePost);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errormainPosts", postDelete);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> deletePost) {

		if (deletePost.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
