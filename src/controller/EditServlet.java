package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		System.out.println((request.getParameter("editId").matches("^[0-9]+$")));

		System.out.println("edit");
		if ((request.getParameter("editId").matches("^[0-9]+$"))) {
			UserService userService = new UserService();
			int editId = Integer.parseInt(request.getParameter("editId"));
			User editUser = userService.getUser(editId);
			if (editUser != null) {

			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			} else {
				System.out.println("fff");
				List<String> messages = new ArrayList<String>();
				messages.add("不正な値です");
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("manage").forward(request, response);
			}
		}else{
			List<String> messages = new ArrayList<String>();
			System.out.println("edit");
			messages.add("不正な値です");
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("manage").forward(request, response);
		}
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		User editUser = getEditUser(request);
		if (isValid(request, messages) == true) {
			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("manage").forward(request, response);
				return;
			}
			response.sendRedirect("manage");
		} else {

			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("/edit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("editId")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password1"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchSelect")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionSelect")));
		editUser.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		int branchId = Integer.parseInt(request.getParameter("branchSelect"));
		int positionId = Integer.parseInt(request.getParameter("positionSelect"));

		if (StringUtils.isBlank(account)) {
			messages.add("アカウント名を入力してください");
		}else{
			if (!(password1.matches("^[0-9a-zA-Z]+$"))) {
				messages.add("入力したアカウントIDは半角英数字で入力してください");
			}else{
				if (!(account.length() >= 6)) {
					messages.add("入力したIDは6文字以上で入力してください");
				}
				if (!(account.length() <= 20)) {
					messages.add("入力したIDは20文字以内で入力してください");
				}
			}

		}
		if (StringUtils.isBlank(password1)) {
			messages.add("パスワードを入力してください");
		}else{
			if (!(password1.matches("^[0-9a-zA-Z]+$"))) {
				messages.add("入力したパスワードは半角英数字で入力してください");
			}
			if (!(password1.length() >= 6)) {
				messages.add("入力したパスワードは6文字以上で入力してください");
			}
			if (!(password1.length() <= 20)) {
				messages.add("入力したパスワードは20文字以内で入力してください");
			}
		}
		if (StringUtils.isBlank(password2)) {
			messages.add("確認パスワードを入力してください");
		}
		if (!(password1.equals(password2))) {
			messages.add("入力したパスワードと確認用パスワードが違います");
		}

		if ((branchId == 1 && !(positionId == 1 || positionId == 2)) || (branchId == 2 && !(positionId == 3 || positionId == 6))
				|| (branchId == 3 && !(positionId == 4 || positionId == 7)) ||  (branchId == 4 && !(positionId == 5 || positionId == 8))) {
			messages.add("入力した支店と役職は正しくありません");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
