package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;;

@WebServlet(urlPatterns = { "/newComment" })
public class CommentServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		if (isValid(request, messages) == true) {
			System.out.println("post開始2");
			User user = (User) session.getAttribute("loginUser");
			Comment comment = new Comment();
			comment.setPostId(Integer.parseInt(request.getParameter("postId")));
			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("text");
		if (StringUtils.isBlank(comment)) {
			messages.add("本文を入力してください");
		}
		if (500 < comment.length()) {
			messages.add("本文を500文字以内で入力してください。");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}