package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;


@WebServlet(urlPatterns = { "/isRebooted" })
public class IsRebootedServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> isRebooted = new ArrayList<String>();
		if (isValid(request, isRebooted) == true) {
			int reboot =Integer.parseInt(request.getParameter("reboot"));
			int rebootId = Integer.parseInt(request.getParameter("rebootId"));
			new UserService().userRebooted(reboot,rebootId);
			response.sendRedirect("manage");
		} else {
			session.setAttribute("errormainComments", isRebooted);
			response.sendRedirect("manage");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> isDeleted) {

		if (isDeleted.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
