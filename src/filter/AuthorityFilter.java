package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")

public class AuthorityFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException,ServletException{
		HttpSession session = ((HttpServletRequest) request).getSession();

		String requestPath = ((HttpServletRequest)request).getServletPath();
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		String managePath = "/manage";
		String editPath = "/edit";
		String signupPath = "/signup";
		String jsp1 ="/manage.jsp";
		String jsp2 ="/edit.jsp";
		String jsp3 ="/signup.jsp";
		if(user != null){
			if(requestPath.equals(managePath) || requestPath.equals(editPath) || requestPath.equals(signupPath)
					||requestPath.equals(jsp1) || requestPath.equals(jsp2) || requestPath.equals(jsp3)){
				int userPositionId = user.getPositionId();
				if (userPositionId != 1 && userPositionId != 2) {
					List<String> messages = new ArrayList<String>();
					messages.add("権限がありません");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse)response).sendRedirect("./");
				}else{
					if (requestPath.equals(jsp1) || requestPath.equals(jsp2) || requestPath.equals(jsp3)) {
						List<String> messages = new ArrayList<String>();
						messages.add("リンクからアクセスしてください");
						session.setAttribute("errorMessages", messages);
						((HttpServletResponse)response).sendRedirect("./");
						return;
					}else{
						chain.doFilter(request, response);
					}
				}
			}else{
				chain.doFilter(request, response);
				return;
			}
		} else {
			chain.doFilter(request, response);
			return;
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy(){}
}