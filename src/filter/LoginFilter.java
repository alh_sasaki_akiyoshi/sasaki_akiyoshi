package filter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")

public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException,ServletException{

		String requestPath = ((HttpServletRequest)request).getServletPath();
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		String path = "/login";
		HttpSession session = ((HttpServletRequest)request).getSession();
		if(requestPath.equals(path)){
			chain.doFilter(request, response);
			return;
		}else if(user != null && session != null){
			chain.doFilter(request, response);
			return;
		}else {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("login");
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy(){}
}