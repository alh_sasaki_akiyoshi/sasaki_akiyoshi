package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {
	public List<Position> getPosition(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("positions.id as id, ");
			sql.append("positions.name as name, ");
			sql.append("positions.created_date as created_date ");
			sql.append("FROM positions ");

			sql.append(" ORDER BY created_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Position> toPositionList(ResultSet rs)
			throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Position position = new Position();

				position.setId(id);
				position.setName(name);

				position.setCreatedDate(createdDate);
				ret.add(position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}