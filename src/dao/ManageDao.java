package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Manage;
import exception.SQLRuntimeException;

public class ManageDao {
	public List<Manage> getManage(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");
			sql.append("users.is_deleted as is_deleted, ");
			sql.append("users.created_date as created_date ");
			sql.append("FROM users ");

			sql.append(" ORDER BY created_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Manage> ret = toManageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Manage> toManageList(ResultSet rs)
			throws SQLException {

		List<Manage> ret = new ArrayList<Manage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId  = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Manage manage = new Manage();

				manage.setId(id);
				manage.setAccount(account);
				manage.setPassword(password);
				manage.setName(name);
				manage.setBranchId(branchId);
				manage.setPositionId(positionId);
				manage.setIsDeleted(isDeleted);

				manage.setCreatedDate(createdDate);
				ret.add(manage);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}