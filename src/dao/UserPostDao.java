package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num, String category, String stert, String end) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.subject as subject,");
			sql.append("posts.category as category,");
			sql.append("posts.text as text, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append(" WHERE posts.created_date >= '" + stert + "' AND posts.created_date < '" + end + "' AND posts.category LIKE '%" + category + "%' ");

			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserPost> toUserPostList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserPost post = new UserPost();

				post.setId(id);
				post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setUserId(userId);
				post.setName(name);

				post.setCreatedDate(createdDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}