package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("id");
			sql.append(", subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, post.getId());
			ps.setString(2, post.getSubject());
			ps.setString(3, post.getText());
			ps.setString(4, post.getCategory());
			ps.setInt(5, post.getUserId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void deletePost(Connection connection, int deletePost) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM posts WHERE id = " + deletePost;
			try {
				ps = connection.prepareStatement(sql.toString());
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} finally {
			close(ps);
		}
	}
}